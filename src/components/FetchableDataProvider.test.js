import React from 'react';
import Enzyme from 'enzyme';
import Faker from 'faker';

import Loader from './Loader';
import { FetchableDataProvider } from "./FetchableDataProviderFor";

describe('FetchableDataProvider Component', () => {
	let wrapper;
	const useEffect = jest.spyOn(React, 'useEffect').mockImplementation(f => f());
	const _defaultProps = {
		fetchData: jest.fn(),
		error: null,
		pending: true,
		data: null,
		children: jest.fn(v => v),
	};
	let props;
	const setUp = (_props = _defaultProps) => {
		props = _props;
		return Enzyme.mount(<FetchableDataProvider {..._props} />);
	};
	beforeEach(() => {
		wrapper = setUp();
	});

	afterEach(() => {
		useEffect.mockClear();
		props.fetchData.mockClear();
	});

	it('should call fetchData prop once', () => {
		expect(props.fetchData).toHaveBeenCalledTimes(1);
	});

	describe('when not pending and error', () => {
		beforeEach(() => {
			wrapper = setUp({
				...props,
				error: 'ERROR MESSAGE',
				pending: false,
			});
		});

		it('should render error text', () => {
			expect(wrapper.text()).toContain(props.error);
		});
	});

	describe('when pending', () => {
		it('should render a Loader', () => {
			expect(wrapper.find(Loader).length).toEqual(1);
		});
	});

	describe('when not pending and no error', () => {
		beforeEach(() => {
			wrapper = setUp({
				...props,
				error: null,
				pending: false,
				data: Faker.random.words(),
			});
		});

		it('should call children function once with data', () => {
			expect(props.children).toHaveBeenNthCalledWith(1, props.data);
		});

		it('should render children function result', () => {
			expect(wrapper.text()).toContain(props.data);
		});
	});
});