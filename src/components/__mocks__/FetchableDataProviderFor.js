import React from 'react';

const mock = jest.genMockFromModule('../FetchableDataProviderFor');

let mockMission = null;
mock.setMission = mission => mockMission = mission;

mock.FetchableDataProvider = ({children}) => {
	return (
		<React.Fragment>
			{children(mockMission)}
		</React.Fragment>
	);
};
mock.default = () => mock.FetchableDataProvider;

module.exports = mock;