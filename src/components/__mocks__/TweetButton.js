import React from 'react';

const mock = jest.genMockFromModule('../TweetButton');

mock.default = () => (<div />);

module.exports = mock;