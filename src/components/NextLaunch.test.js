import React from 'react';
import Enzyme from 'enzyme';
import Faker from "faker";

import NextLaunch from './NextLaunch';
import * as FetchableDataProviderFor from './FetchableDataProviderFor';
import * as TweetButton from './TweetButton';

jest.mock('./FetchableDataProviderFor');
jest.mock('./TweetButton');

describe('LaunchesNext Component', () => {
	let wrapper;
	const _defaultMission = {
		title: Faker.commerce.product(),
		date: Faker.date.soon().toString(),
		details: Faker.commerce.productDescription(),
		img: Faker.image.imageUrl(),
	};
	let mission;
	const setUp = (_mission = _defaultMission) => {
		mission = _mission;
		FetchableDataProviderFor.setMission(_mission);
		return Enzyme.mount(<NextLaunch />);
	};
	beforeEach(() => {
		wrapper = setUp();
	});

	it('should render mission title', () => {
		expect(wrapper.text()).toContain(mission.title);
	});

	it('should render mission date', () => {
		expect(wrapper.text()).toContain(mission.date);
	});

	it('should render mission details', () => {
		expect(wrapper.text()).toContain(mission.details);
	});

	it('should render mission image', () => {
		expect(wrapper.find('img').prop('src')).toStrictEqual(mission.img);
	});

	describe('TweetButton tweet', () => {
		let tweetText;
		beforeEach(() => {
			tweetText = wrapper.find(TweetButton.default).prop('tweet');
		});

		it('should contain mission title', () => {
			expect(tweetText).toContain(mission.title);
		});

		it('should contain mission image', () => {
			expect(tweetText).toContain(mission.img);
		});
	});
});