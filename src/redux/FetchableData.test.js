import Faker from 'faker';
import Nock from 'nock';

import FetchableData from './FetchableData';

const reqUrl = Faker.internet.url();
const name = Faker.random.word().toUpperCase();
const FETCH_REQUEST = `FETCH_${name}_REQUEST`;
const FETCH_FAILURE = `FETCH_${name}_FAILURE`;
const FETCH_SUCCESS = `FETCH_${name}_SUCCESS`;

describe('FetchableData instance', () => {
	let instance;
	const setUp = (dataMiddleware) => {
		instance = new FetchableData({
			name,
			url: reqUrl,
			dataMiddleware: dataMiddleware || jest.fn(v => v),
		})
		return instance;
	};
	beforeEach(() => {
		setUp();
	});

	describe('.action', () => {
		it('should return a function when called', () => {
			expect(typeof(instance.action())).toStrictEqual('function');
		});

		describe('function returned from call', () => {
			const resData = {
				fakeProduct: Faker.commerce.product(),
				fakeNumber: Faker.random.number(),
			};
			const dispatch = jest.fn();
			let retVal;
			let scope;
			const setUpResponse = (resStatus = 200, resBody = resData) => {
				scope = Nock(reqUrl)
					.get('/')
					.reply(resStatus, resBody);
				retVal = instance.action()(dispatch);
				return retVal;
			};
			afterEach(() => {
				dispatch.mockClear();
			});

			it('should dispatch REQUEST then SUCCESS when response is 200 OK', async () => {
				await setUpResponse();

				expect(dispatch.mock.calls).toEqual([
					[{
						type: FETCH_REQUEST,
					}],
					[{
						type: FETCH_SUCCESS,
						data: resData,
					}],
				]);
				scope.done();
			});

			it('should dispatch REQUEST then FAILURE when response is 404 Not found', async () => {
				await setUpResponse(404);

				expect(dispatch.mock.calls).toEqual([
					[{
						type: FETCH_REQUEST,
					}],
					[{
						type: FETCH_FAILURE,
						error: expect.any(Error),
					}],
				]);
			});

			it('should dispatch action with data processed by dataMiddleware', async () => {
				const middlewareAdded = {
					fakeData: Faker.random.words,
				};
				const middleware = jest.fn(data => ({ ...data, ...middlewareAdded }));

				setUp(middleware);
				await setUpResponse();

				expect(middleware).toHaveBeenNthCalledWith(1, resData);
				expect(dispatch).toHaveBeenCalledWith({
					type: `FETCH_${name}_SUCCESS`,
					data: { ...resData, ...middlewareAdded },
				});
			})
		});
	});

	describe('.reducer store state', () => {
		let state;
		beforeEach(() => {
			state = instance.reducer();
		});
		const dispatch = (action) => state = instance.reducer(state, action);

		it('should be valid when nothing was dispatched', () => {
			expect(state).toEqual({
				pending: true,
				error: null,
				data: null,
			});
		});

		it('should be valid after REQUEST dispatch', () => {
			dispatch({
				type: FETCH_REQUEST,
			})
			expect(state).toEqual({
				pending: true,
				error: null,
				data: null,
			});
		});

		it('should be valid after SUCCESS dispatch', () => {
			const data = Faker.random.word();
			dispatch({
				type: FETCH_SUCCESS,
				data,
			})

			expect(state).toEqual({
				pending: false,
				error: null,
				data,
			});
		});

		it('should be valid after FAILURE dispatch', () => {
			const error = new Error(Faker.random.words());
			dispatch({
				type: FETCH_FAILURE,
				error,
			})

			expect(state).toEqual({
				pending: false,
				error,
				data: null,
			});
		});
	});
});