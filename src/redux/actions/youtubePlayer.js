import {
	YOUTUBE_PLAYER_SET_YOUTUBE_ID,
} from 'redux/constants/youtubePlayer'

export const setYoutubeID = youtubeID => ({
	type: YOUTUBE_PLAYER_SET_YOUTUBE_ID,
	youtubeID,
});