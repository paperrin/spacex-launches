describe('App', () => {

	beforeEach(() => {
		cy.visit('http://localhost:3000');
	});

	it('should have the right title', () => {
		cy.title().should('eq', 'SpaceX Launches');
	});

	it('should contain the website title', () => {
		cy.contains('SpaceX Launches');
	});

	it('should contain Recent Activity', () => {
		cy.contains('Recent Activity');
	});

	it('should contain Next Launch', () => {
		cy.contains('Next Launch');
	});
});