describe("NextLaunch's TweetButton", () => {
	beforeEach(() => {
		cy.visit('http://localhost:3000');
	});

	it('should open the twitter page when clicked', () => {
		cy.getTestAttr('NextLaunchTweetButton')
			.invoke('attr', 'href')
			.should('contain', encodeURIComponent('@elonmusk'));
	});
});