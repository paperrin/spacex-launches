describe('YoutubePlayer', () => {
	let ytButtons;

	beforeEach(() => {
		cy.visit('http://localhost:3000');
		ytButtons = cy.getTestAttr('YTButton');
	});

	it('when no yt button was pressed YoutubePlayer should not be visible', () => {
		cy.getTestAttr('YoutubePlayer').should('not.exist');
	});

	it('when a yt button is pressed, it should open', () => {
		cy.getTestAttr('YTButton').first().click();
		cy.getTestAttr('YoutubePlayer').should('exist');
	});

	it('when the close button is pressed, it should close', () => {
		cy.getTestAttr('YTButton').first().click();
		cy.getTestAttr('YoutubePlayerCloseButton').click();
		cy.getTestAttr('YoutubePlayer').should('not.exist');
	});
});