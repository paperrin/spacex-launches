import { applyMiddleware, createStore } from "redux";

import rootReducer from 'redux/reducers/index';
import { middlewares } from 'redux/store';

export const testStore = (initialState) => {
	return applyMiddleware(...middlewares)(createStore);
};